// Ashena Gorgan Mohammadi, 610394128

#include <iostream>
#include <cstdlib>

using namespace std;

void find(int, int, int, string *, string *);
bool isRepeated(int, int, int);
bool isCycle(int, string*);
bool isInTable(string *, string *, int, int);
void print(string *, int);

int *A;

int main()
{
	int n;
	cout << "Enter the number of input strings(n):" << endl;
	cin >> n;
	cout << "Enter the strings:" << endl;
	string *str = new string[n];
	for(int i = 0; i < n; i++)
		cin >> str[i];
	int m;
	cout << "Enter the number of strings in the table(m):" << endl;
	cin >> m;
	cout << "Enter the strings with length n:" << endl;
	string *table = new string[m];
	for(int i = 0; i < m; i++)
		cin >> table[i];
	A = new int[n];
	find(0, n, m, str, table);
	cout << "no" << endl;
	return 0;
}

void find(int i, int n, int m, string *str, string *table)
{
	if(i == n){
		if(isCycle(n, str) && isInTable(table, str, m, n)){
			print(str, n);
			exit(0);
		}
	}
	else{
		for(int j = 0; j < n; j++){
			if(!isRepeated(n, i, j))
				A[i] = j;
			find(i + 1, n, m, str, table);
		}
	}
}

bool isRepeated(int n, int i, int j)
{
	for(int k = 0; k < n; k++)
		if(k != i && A[k] == j)
			return true;
	return false;
}

bool isCycle(int n, string *str)
{
	for(int i = 1; i < n - 1; i++)
		if(str[i - 1][str[i - 1].length() - 1] != str[i][0] &&
				str[i][str[i].length() - 1] != str[i + 1][0])
			return false;
	return (str[n - 1][str[n - 1].length() - 1] == str[0][0]);
}

bool isInTable(string *table, string * str, int m, int n)
{
	string s;
	for(int i = 0; i < n; i++)
		s += str[A[i]][0];
	for(int i = 0; i < m; i++)
		if(s == table[i])
			return true;
	return false;
}

void print(string *str, int n)
{
	for(int i = 0; i < n; i++)
		cout << str[A[i]] << " ";
	cout << endl;
}
