// Ashena Gorgan Mohammadi, 610394128
/* Q : find the longest common subsequence of three given strings A, B, and C.
 * INPUT : three strings A, B, and C
 * OUTPUT : the longest common subsequence of A, B, and C
 * */
// the algorithm as asked is to find LCS of two and find LCS of LCS of the two and the remaining

#include <iostream>
#include <cstring>
#include <string>

using namespace std;

char *printLCS3(string&, string&, string&); // LCS of all
char *lcs(const char *, const char *, int, int); // LCS of two strings
int max(int&, int&);

int main()
{
	string A, B, C;
	cout << "Enter the three strings:" << endl;
	cin >> A >> B >> C;
	cout << "The Longest Common Subsequence of \"" << A << "\", \"" << B << "\" and \"" << C << "\" is:"<< endl;
	cout << printLCS3(A, B, C) << endl;
}

char *printLCS3(string& A, string& B, string& C)
{
	const char *str1 = A.data();
	const char *str2 = B.data();
	const char *str3 = C.data();
	char *sub = lcs(str1, str2, A.size(), B.size());
	char *result = lcs(sub, str3, strlen(sub), C.size());
	return result;
}

char *lcs(const char *A, const char *B, int m, int n)
{
	int dp[m + 1][n + 1];
	for(int i = 0; i <= m; i++)
		for(int j = 0; j <= n; j++){
			if(i == 0 || j == 0)
				dp[i][j] = 0;
			else if(A[i - 1] == B[j - 1])
				dp[i][j] = dp[i - 1][j - 1] + 1;
			else
				dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
		}
	int a = dp[m][n];
	char *ans = new char[a + 1];
	ans[a] = '\0';
	for(int i = m, j = n; i > 0 && j > 0; ){
		if(A[i - 1] == B[j - 1]){
			ans[a - 1] = A[i - 1];
			i--;
			j--;
			a--;
		}
		else if(dp[i - 1][j] > dp[i][j - 1])
			i--;
		else
			j--;
	}
	return ans;
}

int max(int& a, int& b)
{
	int max = a;
	if(b > max)
		max = b;
	return max;
}
