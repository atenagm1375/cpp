// Ashena Gorgan Mohammadi, 610394128

#include <iostream>
#include <vector>

using namespace std;

// The first set(I) is sorted according to the finish time and then with greedy algorithm,
// The first subset of I is set as the first subset of J and we continue by finding the subsets
// that have no overlap with the previous.

struct Job
{
    int start;
    int finish;
};

Job *sortbyFinish(Job *, int&);
vector<Job> greedyJob(Job *, int&);

int main() {
    int n;
    cout << "Enter the number of jobs:" << endl;
    cin >> n;
    Job *I = new Job[n];
    cout << "Enter the start and finish time for each job:" << endl;
    for(int i = 0; i < n; i++){
        cin >> I[i].start;
        cin >> I[i].finish;
    }
    I = sortbyFinish(I, n);
    vector<Job> J = greedyJob(I, n);
    cout << "The length of J is " << J.size() << endl;
    cout << "J is:" << endl;
    for(int i = 0; i < J.size(); i++){
        cout << "start: " << J[i].start << " finish: " << J[i].finish << endl;
    }
    return 0;
}

Job *sortbyFinish(Job *I, int& n)
{
    int j;
    Job a;
    for(int i = 1; i < n; i++){
        a = I[i];
        for(j = i; j > 0 && I[j - 1].finish > a.finish; j--)
            I[j] = I[j - 1];
        I[j] = a;
    }
    return I;
}

vector<Job> greedyJob(Job *I, int& n)
{
    vector<Job> J;
    J.push_back(I[0]);
    for(int i = 1; i < n; i++){
        if(I[i - 1].finish < I[i].start){
            J.push_back(I[i]);
        }
    }
    return J;
}
