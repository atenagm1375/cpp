// Ashena Gorgan Mohammadi, 610394128
/* Q : find the dense subgraphs of a given graph.(we call a graph dense if
 * and only if q/(p*(p-1)/2) > 3/4. q is the number of edges and p is the 
 * the number of vertices.
 * INPUT : number of vertices(n), number of edges(m), pairs of connected vertices
 * OUTPUT : number of vertices and edges of subgraph(p and q), the vertices and 
 * the edges.
 * */
// There is a counter of n^2 in base of 2 representing different states of subgraphs.

#include <iostream>

using namespace std;

void subGraph(int, int *, int&);
int countOnes(int *, int&);
void print(int *, double&, double&, int&);

int *G;

int main()
{
	int n, m;
	cout << "Enter the number of vertices:" << endl;
	cin >> n;
	cout << "Enter the number of edges:" << endl;
	cin >> m;
	G = new int[n * n];
	for(int i = 0; i < n * n; i++)
		G[i] = 0;
	cout << "Enter the connected pairs of vertices:" << endl;
	int a, b;
	for(int i = 0; i < m; i++){
		cin >> a >> b;
		G[a * n + b] = 1;
		G[b * n + a] = 1;
	}
	int *count = new int[n * n];
	subGraph(0, count, n);
	cout << endl;
	return 0;
}

void subGraph(int i, int *H, int& n)
{
	if(i == n * n){
		bool t = true;
		for(int a = 0; t && a < n * n; a++)
			if(H[a] == 1 && H[a] != G[a])
				t = false;
		for(int a = 0; t && a < n; a++)
			for(int b = 0; b < a; b++)
				if(H[a * n + b] != H[b * n + a])
					t = false;
		double q = countOnes(H, n) / 2;
		double density;
		bool f = true;
		for(double p = 2; t && p <= n; p++){
			int counter = 0;
			for(int a = 0; a < n * n; a += n){
				f = true;
				for(int b = a; f && b < a + n; b++){
					if(H[b] == 1){
						f = false;
						counter++;
					}
				}
			}
			if(counter == p){
				density = q / (p * (p - 1) / 2);
				if(density <= 1 && density > 0.75){
					print(H, p, q, n);
				}
			}
		}
	}
	else{
		for(int j = 0; j < 2; j++){
			H[i] = j;
			subGraph(i + 1, H, n);
		}
	}
}

int countOnes(int *H, int& n)
{
	int a = 0;
	for(int i = 0; i < n * n; i++)
		a += H[i];
	return a;
}

void print(int *H, double& p, double& q, int& n)
{
	cout << "p = " << p << " / " << "q = " << q << " / ";
	bool t = true;
	cout << "vertices of subGraph: ";
	for(int i = 0; i < n * n; i += n){
		t = true;
		for(int j = i; t && j < i + n; j++){
			if(H[j] == 1)
				t = false;
		}
		if(!t)
			cout << (i / n) << " ";
	}
	cout << "/ edges: ";
	for(int i = 0; i < n; i++)
		for(int j = 0; j < i; j++)
			if(H[i * n + j] == 1)
				cout << i << "," << j << " ";
	cout << "- ";
}
