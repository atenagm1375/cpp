// Ashena Gorgan Mohammadi, 610394128
/* Q : some coins with different values are given. the aim is to find the minimum
 * number of coins that reach the desired sum.
 * INPUT : integer n representing the number of coins, an array containing the value
 * of each coin, integer s representing the desired summation
 * OUTPUT : a single integer showing the minimum number of coins, or NO if the sum could
 * not reach using the given coins.
 * */
// Since the maximum sum would be 200 and maximum number of coins would be 100, an array
// of s + 1 length will not cause problems for memory. It also seems to be the optimal memory.
// Therefore, an array of s + 1 size is filled by the number of coins reaching the index.

#include <iostream>

using namespace std;

int howManyCoins(int *, int&, int&);

int main()
{
	int n;
	cout << "Enter the number of coins:" << endl;
	cin >> n;
	int *coin = new int[n];
	cout << "Enter the value of each coin:" << endl;
	for(int i = 0; i < n; i++)
		cin >> coin[i];
	int s;
	cout << "Enter your desired value for summation of coins:" << endl;
	cin >> s;
	int answer = howManyCoins(coin, n, s);
	if(answer < 2147483647)
		cout << "The minimum number of coins you might need is " << answer << endl;
	else
		cout << "NO" << endl;
	return 0;
}

int howManyCoins(int *coin, int& n, int& sum)
{
	int *sumValue = new int[sum + 1];
	sumValue[0] = 0;
	for(int i = 1; i < sum + 1; i++)
		sumValue[i] = 2147483647;
	int aid;
	for(int i = 1; i < sum + 1; i++)
		for(int j = 0; j < n; j++)
			if(coin[j] <= i){
				aid = sumValue[i - coin[j]];
				if(aid != 2147483647 && sumValue[i] > aid + 1)
					sumValue[i] = aid + 1;
			}
	return sumValue[sum];
}
