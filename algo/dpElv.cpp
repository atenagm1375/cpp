// Ashena Gorgan Mohammadi, 610394128
/* Q : There are n people who want to get on an elevetor with b kilograms
 * capacity. Each person has specific amount of money which is considered
 * as his/her value and a specific weight, of course:)) Your task is to 
 * choose the passengers in a way that their weights do not surpass the 
 * capacity and they be as valuable as possible.
 * INPUT : capacity of the elevator(b), number of passengers(n), weights,
 	values.
 * OUTPUT : a binary string showing who to get on the elevator
 * */
// As it is asked, the algorithm should be branch and bound. I bound it
// to the maximum value of passengers and the branch changes whenever the
// weights surpasses the capacity or we have fully moved over the array of
// answer.

#include <iostream>

using namespace std;

void Elevator(int, int, int, int&, int&);

int *value, *weight, *answer, *temp, maxValue;

int main()
{
	int b, n;
	cout << "Enter the capacity of Elevator:" << endl;
	cin >> b;
	cout << "Enter the number of passengers:" << endl;
	ci >> n;
	weight = new int[n];
	value = new int[n];
	for(int i = 0; i < n; i++)
		cin >> weight[i];
	for(int i = 0; i < n; i++)
		cin >> value[i];
	answer = new int[n];
	temp = new int[n];
	Elevator(0, 0, 0, b, n);
	for(int i = 0; i < n; i++)
		cout << answer[i];
	cout << endl;
	return 0;
}

void Elevator(int v, int w, int i, int& b, int& n)
{
	if(i == n)
		return;
	if(w > b)
		return;
	if(w <= b && v > maxValue){
		maxValue = v;
		for(int j = 0; j < n; j++)
			answer[j] = temp[j];
	}
	temp[i] = 1;
	Elevator(v + value[i], w + weight[i], i + 1, b, n);
	temp[i] = 0;
	Elevator(v, w, i + 1, b, n);
}
