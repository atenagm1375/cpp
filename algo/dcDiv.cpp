// Ashena Gorgan Mohammadi, 610394128
/* Q : find the multiplication of two numbers using devide & conquer algorithm.
 * INPUT : two numbers n and m showing the number of digits of the two numbers.
 * then each number is enterd using spaces between the digits.
 * OUTPUT : the result of multiplication.
 * */
// The karatsuba multiplication is used.
/* Imagine N and M are two numbers with length n and m. we devide each into two
 * halves so that N would be AB and M would be CD. A*C*(10^(2*len)) + (A*D + B*C)*
 * (10^len) + B*D is the result of the multiplication.
 * */

#include <iostream>
#include <vector>
#include <cstdlib>

using namespace std;

vector<int> multiply(vector<int>, vector<int>);
vector<int> power(vector<int>, int);
vector<int> summation(vector<int>, vector<int>);

int main()
{
	int n, m;
	cout << "Enter number of digits of the two numbers in order:" << endl;
	cin >> n >> m;
	vector<int> num1, num2;
	cout << "Enter the first number(123 : 1 2 3):" << endl;
	int a;
	for(int i = 0; i < n; i++){
		cin >> a;
		num1.push_back(a);
	}
	cout << "Enter the second number(123 : 1 2 3):" << endl;
	for(int i = 0; i < m; i++){
		cin >> a;
		num2.push_back(a);
	}
	if((n == 1 && num1[0] == 0) || (m == 1 && num2[0] == 0)){
		cout << 0;
		exit(0);
	}
	vector<int> answer = multiply(num1, num2);
	for(int i = 0; i < num1.size(); i++)
		cout << num1[i];
	cout <<  " * ";
	for(int i = 0; i < num2.size(); i++)
		cout << num2[i];
	cout << " = ";
	int i = 0;
	while(answer[i] == 0)
		i++;
	for( ; i < answer.size(); i++)
		cout << answer[i];
	cout << endl;
	return 0;
}

vector<int> multiply(vector<int> num1, vector<int> num2)
{
	if(num1.size() == 1 && num2.size() == 1){
		vector<int> ans;
		if(num1[0] * num2[0] < 10)
			ans.push_back(num1[0] * num2[0]);
		else{
			ans.push_back((num1[0] * num2[0]) / 10);
			ans.push_back((num1[0] * num2[0]) % 10);
		}
		return ans;
	}
	
	int len = num1.size() <= num2.size() ? num1.size() / 2 : num2.size() / 2;
	if(len == 0)
		len++;
	vector<int> A, B, C, D;
	int i = 0;
	for( ; i < num1.size() - len; i++)
		A.push_back(num1[i]);
	for( ; i < num1.size(); i++)
		B.push_back(num1[i]);
	if(A.size() == 0)
		A.push_back(0);
	i = 0;
	for( ; i < num2.size() - len; i++)
		C.push_back(num2[i]);
	for( ; i < num2.size(); i++)
		D.push_back(num2[i]);
	if(C.size() == 0)
		C.push_back(0);
	return summation(summation(power(multiply(A, C), 2 * len), multiply(B, D)),
						power(summation(multiply(A, D), multiply(B, C)), len));
}

vector<int> power(vector<int> num, int p)
{
	for(int i = 0; i < p; i++)
		num.push_back(0);
	return num;
}

vector<int> summation(vector<int> A, vector<int> B)
{
	while(A.size() < B.size())
		A.insert(A.begin(), 0);
	while(B.size() < A.size())
		B.insert(B.begin(), 0);
	int add = 0;
	vector<int> ans;
	for(int i = A.size() - 1; i >= 0; i--){
		int current = A[i] + B[i] + add;
		ans.insert(ans.begin(), current % 10);
		add = current / 10;
	}
	if(add != 0)
		ans.insert(ans.begin(), add);
	return ans;
}
