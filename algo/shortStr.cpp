// Ashena Gorgan Mohammadi, 610394128
/* Q : find the shortest common binary string among a number of binary strings
 * INPUT : number of strings(n) and the strings. The maximum total summation of
 * the length of strings would be 25
 * OUTPUT : The shortest common string
 * */
// using a counter, strings are created and it is checked if it contains all the
// input strings or not.

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

void shortestCommonStr(int, string *, int);
bool doesContain(string *, int);

int n;
int *commonStr;

int main()
{
	cout << "Enter the number of binary strings:" << endl;
	cin >> n;
	string *binaryStr = new string[n];
	cout << "Enter the binary strings(The maximum summation of their lengths would be 25):" << endl;
	for(int i = 0; i < n; i++)
		cin >> binaryStr[i];
	int max = 0;
	int count = 0;
	for(int i = 0; i < n; i++){
		count += binaryStr[i].length();
		if(binaryStr[i].length() > max)
			max = binaryStr[i].length();
	}
	for(int i = max; i <= count; i++){
		commonStr = new int[i];
		shortestCommonStr(0, binaryStr, i);
	}
}

void shortestCommonStr(int i, string *str, int L)
{
	if(i == L){
		if(doesContain(str, L)){
			for(int k = 0; k < L; k++)
				cout << commonStr[k];
			exit(0);
		}
	}
	else{
		for(int j = 0; j < 2; j++){
			commonStr[i] = j;
			shortestCommonStr(i + 1, str, L);
		}
	}
}

bool doesContain(string *str, int L)
{
	int count = 0;
	for(int i = 0; i < n; i++){
		for(int j = 0; j <= L - str[i].length(); j++)
			if(str[i][0] == (commonStr[j] + '0')){
				bool t = true;
				for(int k = 0; t && k < str[i].length(); k++)
					if(str[i][k] != (commonStr[j + k] + '0'))
						t = false;
				if(t){
					count++;
					j = L;
				}
			}
	}
	return count == n;
}
