//Atena G.Mohammadi
/*Description
The teacher gave Anton a large geometry homework, but he didn't do it (as usual) as he participated in a regular round on Codeforces. In the task he was given a set of n lines defined by the equations y = ki·x + bi. It was necessary to determine whether there is at least one point of intersection of two of these lines, that lays strictly inside the strip between x1 < x2. In other words, is it true that there are 1 ≤ i < j ≤ n and x', y', such that:

y' = ki * x' + bi, that is, point (x', y') belongs to the line number i;
y' = kj * x' + bj, that is, point (x', y') belongs to the line number j;
x1 < x' < x2, that is, point (x', y') lies inside the strip bounded by x1 < x2.
You can't leave Anton in trouble, can you? Write a program that solves the given task.

Input
The first line of the input contains an integer n (2 ≤ n ≤ 100 000) — the number of lines in the task given to Anton. The second line contains integers x1 and x2 ( - 1 000 000 ≤ x1 < x2 ≤ 1 000 000) defining the strip inside which you need to find a point of intersection of at least two lines.

The following n lines contain integers ki, bi ( - 1 000 000 ≤ ki, bi ≤ 1 000 000) — the descriptions of the lines. It is guaranteed that all lines are pairwise distinct, that is, for any two i ≠ j it is true that either ki ≠ kj, or bi ≠ bj.

Output
Print "Yes" (without quotes), if there is at least one intersection of two distinct lines, located strictly inside the strip. Otherwise print "No" (without quotes).

Sample Input
Input
4
1 2
1 2
1 0
0 1
0 2
Output
NO
Input
2
1 3
1 0
-1 3
Output
YES
Input
2
1 3
1 0
0 2
Output
YES
Input
2
1 3
1 0
0 3
Output
NO*/

#include <iostream>
#include <algorithm>
#include <utility>

using namespace std;

int main(){
	long long n, x1, x2;
	cin >> n >> x1 >> x2;
	pair<long long, long long> P[n];
	for(int i = 0; i < n; i++){
		long long k, b;
		cin >> k >> b;
		P[i] = make_pair(k * x1 + b, k * x2 + b);
	}
	sort(P, P + n);
	bool t = false;
	for(int i = 0; !t && i < n - 1; i++)
		if(P[i].second > P[i + 1].second)
			t = true;
	cout << (t ? "Yes" : "No");
	return 0;
}
