//Atena G.Mohammadi
/*Description
One day, as Sherlock Holmes was tracking down one very important criminal, he found a wonderful painting on the wall. This wall could be represented as a plane. The painting had several concentric circles that divided the wall into several parts. Some parts were painted red and all the other were painted blue. Besides, any two neighboring parts were painted different colors, that is, the red and the blue color were alternating, i. e. followed one after the other. The outer area of the wall (the area that lied outside all circles) was painted blue. Help Sherlock Holmes determine the total area of red parts of the wall.

Let us remind you that two circles are called concentric if their centers coincide. Several circles are called concentric if any two of them are concentric.

Input
The first line contains the single integer n (1 ≤ n ≤ 100). The second line contains n space-separated integers ri (1 ≤ ri ≤ 1000) — the circles' radii. It is guaranteed that all circles are different.

Output
Print the single real number — total area of the part of the wall that is painted red. The answer is accepted if absolute or relative error doesn't exceed 10 - 4.

Sample Input
Input
1
1
Output
3.1415926536
Input
3
1 4 2
Output
40.8407044967*/

#include <iostream>
#include <algorithm>
#include <cmath>
#include <iomanip>

using namespace std;

struct myclass {
  bool operator() (int i,int j) { return (i > j);}
} isLarger;

int main(){
	int n;
	cin >> n;
	int *C = new int[n];
	for(int i = 0; i < n; i++)
		cin >> C[i];
	sort(C, C + n, isLarger);
	double S = 0;
	for(int i = 0; i < n; i++){
		S += pow(-1, i) * C[i] * C[i] * 3.1415926536;
	}
	cout << setprecision(10)  << S << endl;
	return 0;
}
