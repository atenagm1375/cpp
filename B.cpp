//Atena G.Mohammadi
/*Description
Sereja has an array a, consisting of n integers a1, a2, ..., an. The boy cannot sit and do nothing, he decided to study an array. Sereja took a piece of paper and wrote out m integers l1, l2, ..., lm (1 ≤ li ≤ n). For each number li he wants to know how many distinct numbers are staying on the positions li, li + 1, ..., n. Formally, he want to find the number of distinct numbers among ali, ali + 1, ..., an.?

Sereja wrote out the necessary array elements but the array was so large and the boy was so pressed for time. Help him, find the answer for the described question for each li.

Input
The first line contains two integers n and m (1 ≤ n, m ≤ 105). The second line contains n integers a1, a2, ..., an (1 ≤ ai ≤ 105) — the array elements.

Next m lines contain integers l1, l2, ..., lm. The i-th line contains integer li (1 ≤ li ≤ n).

Output
Print m lines — on the i-th line print the answer to the number li.

Sample Input
Input
10 10
1 2 3 4 1 2 3 4 100000 99999
1
2
3
4
5
6
7
8
9
10
Output
6
6
6
6
6
5
4
3
2
1*/
#include <iostream>
#include <algorithm>

using namespace std;

int main(){
	int n, m;
	cin >> n >> m;
	int *a = new int[n];
	int *l = new int[m];
	for(int i = 0; i < n; i++)
		cin >> a[i];
	for(int i = 0; i < m; i++)
		cin >> l[i];
	bool *rep = new bool[*max_element(a, a + n)];
	for(int i = 0; i < n; i++)
		rep[a[i]] = true;
	int *result = new int[n];
	result[n - 1] = 1;
	for(int i = n - 2; i >= 0; i--){
		result[i] = result[i + 1];
		if(rep[a[i]]){
			result[i]++;
			rep[a[i]] = false;
		}
	}
	for(int i = 0; i < m; i++)
		cout << result[l[i] - 1] << endl;
	return 0;
}
